import React, { useState, useEffect } from 'react';
import {AppBar, Toolbar, useScrollTrigger, Tabs, Tab, Button} from '@material-ui/core';
import { Menu, MenuItem } from "@material-ui/core";
import {Link} from "react-router-dom";
import {makeStyles } from "@material-ui/styles";
import logo from "assets/logo.svg"

const useStyles = makeStyles( theme => ({
    toolbarMargin: {
        ...theme.mixins.toolbar,
        marginBottom: '0.5rem'
    },
    logo: {
        height: '4.5rem'
    },
    tab: {
        ...theme.typography.tab
    },
    button: {
        borderRadius: '1rem',
        marginLeft:'1rem',
        marginRight:'1rem'
    },
    menu: {
        backgroundColor: theme.palette.primary.main,
        color: 'white',
        borderRadius: '0px'
    }
}));

function ElevationScroll(props) {
    const { children } = props;
    const trigger = useScrollTrigger({
        disableHysteresis: true,
        threshold: 0,
    });
    return React.cloneElement(children, {
        elevation: trigger ? 4 : 0,
    });
}


const Header = ()=>{
    const classes = useStyles();
    const [tab, setTab] = useState(0);
    const [anchorEl, setAnchorEl] = useState(null);
    const [open, setOpen] = useState(false);

    const handleChange = (event, value) => {
        setTab(value);
    };
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
        setOpen(true);
    };
    const handleClose = (event) => {
        setAnchorEl(null);
        setOpen(false);
    };

    useEffect(()=>{
        if (window.location.pathname === "/" && tab!==0) {
            setTab(0);
        }
        else if (window.location.pathname === "/components" && tab!==1) {
            setTab(1)
        }
        else if (window.location.pathname === "/about" && tab!==2) {
            setTab(2)
        }
        else if (window.location.pathname === "/contact" && tab!==3) {
            setTab(3)
        }


            });
    return (
        <React.Fragment>
        <ElevationScroll>
        <AppBar position="fixed" color="primary">
            <Toolbar disableGutters>
                <img className={classes.logo} src={logo} alt="logo"/>
                <Tabs
                    indicatorColor="secondary"
                    onChange={handleChange}
                    value={tab}>
                    <Tab className={classes.tab} component={Link} to="/" label="Home"/>
                    <Tab
                        aria-owns={ anchorEl ? "simple-menu" : undefined}
                        aria-haspopup={ anchorEl ? "true": undefined}
                        onMouseOver={e=>handleClick(e)}
                        className={classes.tab}
                        component={Link}
                        to="/components" label="Components"/>
                    <Tab className={classes.tab} component={Link} to="/about" label="About"/>
                    <Tab className={classes.tab} component={Link} to="/contact" label="Contact"/>
                </Tabs>
                <Button
                    className={classes.button}
                    variant="contained" color="secondary">
                    Free Estimate
                </Button>
                <Menu
                    id="simple-menu" anchorEl={anchorEl}
                    open={open}
                    MenuListProps={{onMouseLeave: handleClose}}
                    classes={{paper: classes.menu}}
                    elevation={0}
                    onClose={handleClose}>
                    <MenuItem
                        component={Link} to="/components/questionnaire"
                        onClick={()=>{handleClose(); setTab(1)}}>Questionnaire</MenuItem>
                    <MenuItem onClick={handleClose}>Test2</MenuItem>
                    <MenuItem onClick={handleClose}>Test3</MenuItem>
                </Menu>

            </Toolbar>
        </AppBar>
        </ElevationScroll>
        <div className={classes.toolbarMargin}/>
        </React.Fragment>

    )
}

export default Header;
