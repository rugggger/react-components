import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Questionnaire from "components/QuestionsTree/Questionnaire";
import { ThemeProvider } from "@material-ui/styles";
import  theme from "ui/Theme";
import Header from "layout/header/Header";


function App() {
  return (
    <ThemeProvider theme={theme}>
        <BrowserRouter>
            <Header/>
            <Switch>
                <Route exact path="/" component={()=>{return <div>home</div>}}/>
                <Route exact path="/about" component={()=>{return <div>about</div>}}/>
                <Route exact path="/components" component={()=>{return <div>Components</div>}}/>
                <Route exact path="/contact" component={()=>{return <div>contact</div>}}/>
                <Route exact path="/estimate" component={()=>{return <div>estimate</div>}}/>
                <Route exact path="/components/questionnaire" component={Questionnaire}/>
            </Switch>
        </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
