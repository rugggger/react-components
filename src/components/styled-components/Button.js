import styled, {css} from 'styled-components';

const Button = styled.button`
    width:100%;
    color: white;
    background: ${props => props.secondary ? 'red' : '#f8049c'};
    ${props => props.large ? css`
      font-size: 1.5rem;
      padding: 10px;
      border-radius: 10px;

    `: 
    css`
      font-size: 1rem;
      padding: 8px;
       border-radius: 5px;
    `}
    font-weight: bold;
    display: block;
    border: none;
    &:disabled {
      background: #eee;
    }    
    
`;

export {Button};
