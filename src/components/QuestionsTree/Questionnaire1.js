
const Questionnaire1 = {
    question: "Question 1 ?",
    id: "question-1",
    options: [
        {label:"answer1", value:1,
            question:{
              question: "Question 2",
              id: "question-2",
              options: [
                  {label:"answer4", value:4},
                  {label:"answer5", value:5}
              ]
            }
        },
        {label:"answer2", value:2,
            question:{
                question: "Question 3",
                id: "question-3",
                options: [
                    {label:"answer6", value:6,
                        question:{
                            question: "Question 4",
                            id: "question-4",
                            options: [
                                {label:"answer8", value:8},
                                {label:"answer9", value:9}
                            ]
                        }
                    },
                    {label:"answer7", value:7}
                ]
            }
        },
        {label:"answer3", value:3}]
};

export default Questionnaire1;
