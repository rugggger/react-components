import React from 'react';
import { Button } from "@material-ui/core";
import { ToggleButtonGroup, ToggleButton } from "@material-ui/lab";

const Question = (props)=>{
    const {question, options, initialValue, answerReceived, questionId} = props;

    const [value, setValue] = React.useState(initialValue);

    const handleChange = (event, newVal) => {
        if (newVal !== null) {
            setValue(newVal);
            const answer = {
                [questionId]: newVal
            };
            answerReceived(answer);
        }

    };

    const button_group = options.map(option =>
        <ToggleButton key={option.value} value={option.value}>{option.label}</ToggleButton>);

    return (

        <div>
            <h2>{question}</h2>
            <ToggleButtonGroup
                value={value}
                exclusive
                onChange={handleChange}
                variant="contained" color="primary" aria-label="contained primary button group">
                {button_group}
            </ToggleButtonGroup>
        </div>
    );

};

export default Question;
