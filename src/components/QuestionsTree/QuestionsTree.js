import React from 'react';
import Question from "./Question";
import {Button} from "@material-ui/core";

const QuestionsTree = (props) => {
    const {questionnaire, answers, onChange, onContinue} = props;


    const handleAnswerClicked = (answer) => {
        const newAnswers = Object.assign({}, answers, answer);
        onChange(newAnswers);
    };


    const handleAnswer = (answer)=>{
        onChange(answer);
    };

    let moreQuestions = null;
    let button = null;

    const initialValue = answers[questionnaire.id];

    let selected = questionnaire.options.find(option=>option.value === answers[questionnaire.id]);
    if (selected && selected.question){
            moreQuestions = <QuestionsTree
                       questionnaire={selected.question}
                       answers={answers}
                       onChange={handleAnswerClicked}
                       onContinue={onContinue}
            />
    }
    if (selected && !selected.question) {
        button = <Button
                    style={{marginTop:'1rem'}}
                    variant="contained"
                    onClick={onContinue}
                   >Continue</Button>
    }



    return (
        <div>
            <Question
                answerReceived={handleAnswer}
                initialValue={initialValue}
                question={questionnaire.question}
                questionId={questionnaire.id}
                options={questionnaire.options}
            />
            {moreQuestions}
            {button}
        </div>
    );
};

export default QuestionsTree;
