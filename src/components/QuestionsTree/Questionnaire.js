import React, { useState } from 'react';
import Questionnaire1 from "./Questionnaire1";
import QuestionsTree from "./QuestionsTree";

const Questionnaire = ()=>{

    const [answers,setAnswers] = useState({});


    const onChange = (answerReceived) => {
        setAnswers(answerReceived);
    };
    const onContinue = ()=>{
        console.log('clicked continue ', answers);
    };

    return (
        <div>
            <h2>questionnaire</h2>
            {JSON.stringify(answers)}
            <QuestionsTree
                questionnaire={Questionnaire1}
                answers={answers}
                onChange={onChange}
                onContinue={onContinue}
            />


        </div>
    );

};

export default Questionnaire;
