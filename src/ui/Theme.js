import { createMuiTheme } from '@material-ui/core/styles';

const myBlue = "#0B72B9";
const myOrange = "#FFBA60";

export default createMuiTheme({

    palette: {
        primary: {
            main: `${myBlue}`
        },
        secondary: {
          main: `${myOrange}`
        }

    },
    typography: {
        h3: {
            fontWeight: 300
        },
        tab: {
            textTransform: 'none',
            fontWeight: 700,
            fontSize: '1rem'

        }

      //  fontSize: 20
    }
});


